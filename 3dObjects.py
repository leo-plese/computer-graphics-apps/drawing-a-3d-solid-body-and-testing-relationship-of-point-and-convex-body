
from os import path

from pyglet.gl import *
from pyglet.window import key
from pyglet.window import mouse


vertexDefs=[]
polyDefs=[]

width = 700
height = 700

# otvara prozor
config = pyglet.gl.Config(double_buffer=False)
window = pyglet.window.Window(width=width, height=height, caption="3d object", resizable=False, config=config, visible=False)
window.set_location(100, 100)


#+
@window.event
def on_draw():
    vs = scaleObject(vertexDefs, 300)
    vs = translateObject(vs, width/2, height/2, 0)


    #print("vertdefs=",vertexDefs)
    glBegin(GL_POINTS)
    for v in vertexDefs:
        glVertex2f(v[0], v[1])
    glEnd()

    glBegin(GL_LINES)

    for p in polyDefs:
        point0 = vs[p[0] - 1]
        point1 = vs[p[1] - 1]
        point2 = vs[p[2] - 1]

        glVertex2f(point0[0], point0[1])
        glVertex2f(point1[0], point1[1])

        glVertex2f(point0[0], point0[1])
        glVertex2f(point2[0], point2[1])

        glVertex2f(point1[0], point1[1])
        glVertex2f(point2[0], point2[1])

    glEnd()

    glFlush()


@window.event
def on_resize(w, h):
    global width, height
    width = w
    height = h
    global Ix
    Ix = 0
    glViewport(0, 0, width, height)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(0, width, 0, height)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glClearColor(1.0, 1.0, 1.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glPointSize(1.0)
    glColor3f(0.0, 0.0, 0.0)



def loadObjectData(objectDescr):
    vertexNum = 0
    global vertexDefs
    global polyDefs
    polyNum = 0
    for line in objectDescr:
        if line.startswith("v"):
            print(line, end='')
            vertexNum += 1
            vertexDefs.append(line.split()[1:])
        elif line.startswith("f"):
            print(line, end='')
            polyNum += 1
            polyDefs.append(line.split()[1:])

    vertexDefs = [list(map(float, x)) for x in vertexDefs]
    polyDefs = [list(map(int, x)) for x in polyDefs]

def getCenterRange(vs):
    xCoords = [v[0] for v in vs]
    yCoords = [v[1] for v in vs]
    zCoords = [v[2] for v in vs]

    xmin, xmax, ymin, ymax, zmin, zmax = min(xCoords), max(xCoords), min(yCoords), max(yCoords), min(zCoords), max(
        zCoords)

    xCenter = (xmin + xmax) / 2
    yCenter = (ymin + ymax) / 2
    zCenter = (zmin + zmax) / 2


    maxRange = max(xmax-xmin, ymax-ymin, zmax-zmin)
    #print("C({}, {}, {})".format(xCenter, yCenter, zCenter))
    #print("MAX = ", maxRange)

    return xCenter, yCenter, zCenter, maxRange

def translateObject(vertices, dx, dy, dz):
    return [[v[0]+dx, v[1]+dy, v[2]+dz] for v in vertices]

def scaleObject(vertices, scaleFactor):
    return [[v[0]*scaleFactor, v[1]*scaleFactor, v[2]*scaleFactor] for v in vertices]

def enterPointCoordinate(pntCoordName):
    while True:
        try:
            return float(input(pntCoordName))
        except ValueError:
            print("Koordinate trebaju biti realni brojevi.")


def enterTestPoint():
    print("Upisite koordinate ispitne tocke V: ")
    xTest = enterPointCoordinate("x = ")
    yTest = enterPointCoordinate("y = ")
    zTest = enterPointCoordinate("z = ")

    return xTest, yTest, zTest

def enterObjFilename():
    objFilename = input("Ime datoteke objekta za ucitati: ")
    objFilepath = "./" + objFilename
    while not(path.exists(objFilepath) and objFilepath.endswith(".obj")):
        print("Datoteka " + objFilename + " ne postoji.")
        objFilename = input("Ime datoteke objekta za ucitati: ")
        objFilepath = "./" + objFilename

    return objFilepath


def calcPlaneEquations():
    planeCoeffs = []
    for p in polyDefs:
        point1 = vertexDefs[p[0] - 1]
        point2 = vertexDefs[p[1] - 1]
        point3 = vertexDefs[p[2] - 1]
        a = (point2[1]-point1[1])*(point3[2]-point1[2]) - (point2[2]-point1[2])*(point3[1]-point1[1])
        b = -(point2[0]-point1[0])*(point3[2]-point1[2]) + (point2[2]-point1[2])*(point3[0]-point1[0])
        c = (point2[0]-point1[0])*(point3[1]-point1[1]) - (point2[1]-point1[1])*(point3[0]-point1[0])
        d = -a * point1[0] - b * point1[1] - c * point1[2]
        #d = -a * point2[0] - b * point2[1] - c * point2[2]
        #d = -a * point3[0] - b * point3[1] - c * point3[2]
        planeCoeffs.append((a, b, c, d))

    return planeCoeffs

def checkTestPointWithinBody(testPnt, planeCoeffs):
    for plane in planeCoeffs:
        r = plane[0] * testPnt[0] + plane[1] * testPnt[1] + plane[2] * testPnt[2] + plane[3]
        #print("r=",r)
        if r > 0:
            return False
    return True

def checkConvex(planeCoeffs):
    for plane in planeCoeffs:
        n = len(vertexDefs)
        i = 0
        first_sgn = None
        while i < n:
            pointNums = polyDefs[i]
            while (i+1) in pointNums:
                i += 1
            if i == n-1:
                break
            i += 1
            v = vertexDefs[i]
            r = plane[0] * v[0] + plane[1] * v[1] + plane[2] * v[2] + plane[3]
            if first_sgn is None:
                first_sgn = 1 if r > 0 else 0
            else:
                cur_sgn = 1 if r > 0 else 0
                if cur_sgn != first_sgn:
                    return False
    return True

if __name__ == "__main__":
    objFilepath = enterObjFilename()

    # komentari s objektima na kojima je program testiran (neki su vlastito izradeni radi testiranja)
    #with open("./all.obj", "r") as objectDescr:
    #with open("./arena.obj", "r") as objectDescr:
    #with open("./dragon.obj", "r") as objectDescr:
    #with open("./tsd00.obj", "r") as objectDescr:
    #with open("./skull.obj", "r") as objectDescr:
    #with open("./bull.obj", "r") as objectDescr:
    #with open("./porsche.obj", "r") as objectDescr:
    #with open("./frog.obj", "r") as objectDescr:
    #with open("./bird.obj", "r") as objectDescr:
    #with open("./teddy.obj", "r") as objectDescr:
    #with open("./teapot.obj", "r") as objectDescr:
    #with open("./kocka.obj", "r") as objectDescr:
    #with open("./tetrahedron.obj", "r") as objectDescr:
    #with open("./kvadar.obj", "r") as objectDescr:
    with open(objFilepath, "r") as objectDescr:
        loadObjectData(objectDescr)

    xc, yc, zc, maxRange = getCenterRange(vertexDefs)

    print()
    #print(vertexDefs)
    print("Translacija sredista objekta u ishodiste...")
    vertexDefs = translateObject(vertexDefs, -xc, -yc, -zc)
    #print(vertexDefs)

    print("Skaliranje na [-1, 1]...")
    vertexDefs = scaleObject(vertexDefs, 2/maxRange)
    #print(vertexDefs)

    print()
    print("Translatiran i skaliran objekt:")
    for v in vertexDefs:
        print("v {} {} {}".format(*v))
    for p in polyDefs:
        print("f {} {} {}".format(*p))

    testPnt = enterTestPoint()
    print(testPnt)

    planeCoeffs = calcPlaneEquations()
    if not checkConvex(planeCoeffs):
        print("Tijelo je konkavno. Izračun položaja ispitne točke prema poligonu neće biti valjan!")
    else:
        print("Tijelo je konveksno.")

    if checkTestPointWithinBody(testPnt, planeCoeffs):
        print("Tocka V je unutar tijela!")
    else:
        print("Tocka V je IZVAN tijela!")


    window.set_visible(True)

    pyglet.app.run()