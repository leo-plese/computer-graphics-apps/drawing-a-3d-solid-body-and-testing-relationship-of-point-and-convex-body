Drawing a 3D Solid Body and Testing Relationship of Point and Convex Body. Implemented in Python using pyglet, Python OpenGL interface.

My lab assignment in Interactive Computer Graphics, FER, Zagreb.

Docs in "DokumentacijaIRGLabosi" under "4. VJEZBA."

Created: 2020
